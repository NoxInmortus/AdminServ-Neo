# AdminServ-Neo
### What is AdminServ-Neo?
>>>
AdminServ-Neo is your favorite web-based RPC tool for ManiaPlanet servers.

From adding maps, changing match settings to keeping an eye on current players,
AdminServ-Neo is here for you and allows you almost limitless control.
>>>

### Features
* Permission-based access - Full control over who can do what on which server
* ManiaPlanet authentication - No sharing of SuperAdmin passwords, yay!
* Extendable - Know Laravel? Then you can write plugins for this!
* Mania-Exchange support - Add maps quickly without having to upload any files
* Update notifier - If there's a new update for AS-Neo, it'll let you know

### Current Requirements
* MySQL/MariaDB - any recent version should do
* Composer
* Apache web server
* PHP 7.1.3 or newer
  * OpenSSL PHP Extension
  * PDO PHP Extension
  * Mbstring PHP Extension
  * Tokenizer PHP Extension
  * XML PHP Extension
  * MySQLi PHP Extension


### Install Instructions
1. Download `composer.phar` from https://getcomposer.org/download/ (Use Manual Download) into the AdminServ-Neo folder.
2. `php composer.phar install`
3. Rename `.env.example` to `.env`
4. `php artisan key:generate`
5. Create a new Apache vhost pointing to /path/to/adminserv-neo/public 
6. MOD_REWRITE HAS TO BE ENABLED ON THE VHOST FOR ADMINSERV TO WORK
7. Navigate to https://insertyour.vhosturlhere.tld
8. Complete the setup in your browser.

### FAQ
##### :question: Wait, what's different compared to AdminServ?
>>>
AdminServ-Neo is a complete rewrite and reimagination of AdminServ! 

Based on Laravel 5, this gives us a solid ground for everything we wanted to do and potential for more features in the future.

Since we started from scratch, we decided to **drop support** for any prior TrackMania titles that aren't part of the ManiaPlanet environment, sorry!
>>>

##### :question: I found a bug, how can I report it?
>>>
Please open an [issue](https://gitlab.com/Chris92de/AdminServ-Neo/issues) with as much information as possible, preferably reproduction steps and stacktraces.
>>>

##### :question: Where can I follow the development?
>>>
Join the official AdminServ-Neo [Discord Server](https://discord.gg/UmxBw9V) or follow the official [Twitter account](https://twitter.com/AdminServ)
>>>

##### :question: I like this project, can I support its development somehow?
>>>
You can support the continued development process by either creating pull requests for features you added yourself or [buy me a beer](https://twitch.streamlabs.com/chris92)!
>>>

### Credits
* Reaby - did most of the heavy lifting to get this rewrite off the ground. You're awesome, dude, thanks so much!
* ChargerHellcatDallas - thanks for your generous tip!
* *to be continued*



