<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('host')->nullable();
            $table->integer('port')->nullable();
            $table->string('user')->nullable();
            $table->string('pass')->nullable();
            $table->boolean("useRemote")->nullable()->default(false);
            $table->boolean("useSftp")->nullable();
            $table->string('ftpHost')->nullable();
            $table->integer('ftpPort')->nullable();
            $table->string('ftpUser')->nullable();
            $table->string('ftpPass')->nullable();
            $table->timestamps();
        });

        Schema::create('server_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('server_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('server_user');
        Schema::dropIfExists('servers');
    }
}
