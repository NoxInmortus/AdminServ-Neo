<?php

namespace App;

use App\Observers\ServerObserver;
use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $fillable = ["name", "description", "user", "pass", "host", "port",
        "useRemote", "useSftp", "ftpUser", "ftpPass", "ftpHost", "ftpPort",
    ];

    protected $casts = [
        "port" => "integer",
        "useRemote" => "boolean",
        "useSftp" => "boolean",
        "ftpPort" => "integer"
    ];

    public static function boot()
    {
        parent::boot();
        Server::observe(new ServerObserver());
    }


    public function users()
    {
        return $this->belongsToMany("App\User", "server_user", null, null, null, null, null);
    }

    public function roles()
    {
        return $this->hasMany('App\Role', "server_id", "id");
    }

    public static function getUserServers($uid)
    {
        return Server::whereHas('users', function ($query) use ($uid) {
            return $query->where("user_id", $uid);
        });
    }

}
