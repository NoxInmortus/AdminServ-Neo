<?php

namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    protected $fillable = ["name", "display_name", "description", 'server_id'];

    public function server()
    {
        return $this->hasOne("App\Server", "id", "server_id");
    }

    public function users()
    {
        return $this->belongsToMany('App\User', "role_user", null, null, null, null, null);
    }

    public static function forServer($id)
    {
        return Role::on()->where("server_id", "=", $id);
    }

    /**
     * @return Role
     */
    public static function getAdminserv()
    {
        return Role::on()->where("server_id", -1);
    }

}
