<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;


class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'login', 'path', 'nickname', 'active', 'locale'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function servers()
    {
        return $this->belongsToMany("App\Server", "server_user", null, null, null, null, null);
    }

    /**
     * Has user access to server
     * @param Server|int $sid
     * @return boolean
     */
    public function hasAccessToServer($sid)
    {
        if ($sid instanceof Server) {
            $sid = $sid->id;
        }

        return $this->servers()->where('server_id', $sid)->exists();
    }

    public function isMasterAdmin()
    {
        return $this->roles()->where("server_id", -1)->where("name", "adminserv-master")->exists();
    }
}
