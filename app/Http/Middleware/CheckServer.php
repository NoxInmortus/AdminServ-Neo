<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckServer
{
    /**
     * Check if user has permission for this server
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param \App\Server $sid
     * @return mixed
     */
    public function handle($request, Closure $next, $sid)
    {
        if(Auth::user()->hasAccessToServer($sid) || Auth::user()->isMasterAdmin()) {
            return $next($request);
        }
        return redirect()->back()->with('error', 'You do not have permission to access this page');
    }
}