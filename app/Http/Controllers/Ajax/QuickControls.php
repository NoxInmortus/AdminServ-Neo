<?php

namespace App\Http\Controllers\Ajax;

use App\Helpers\Maniaplanet;
use App\Http\Controllers\Controller;

class QuickControls extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function index($id)
    {
        try {
            $connection = Maniaplanet::connect($id);
            return view("server.ajax.quickControls", compact('id'))->with('state', 'enabled');
        } catch(\Exception $ex){
            return view("server.ajax.quickControls", compact('id'))->with('state', 'disabled');
        }

    }

    public function restartMap($id)
    {
        $connection = Maniaplanet::connect($id);
        $connection->restartMap();

        if (Maniaplanet::returnCallback($connection, "ManiaPlanet.BeginMatch")) {
            return redirect()->route('server.manage', $id)->with("success", "Restarted current map!");
        } else {
            return redirect()->route('server.manage', $id)->with("error", "Unknown error");
        }
    }

    public function skipMap($id)
    {
        $connection = Maniaplanet::connect($id);
        $connection->nextMap();

        if (Maniaplanet::returnCallback($connection, "ManiaPlanet.EndMatch")) {
            return redirect()->route('server.manage', $id)->with("success", "Skipped current map!");
        } else {
            return redirect()->route('server.manage', $id)->with("error", "Unknown error");
        }
    }

    public function forceEndRound($id)
    {
        $connection = Maniaplanet::connect($id);

        if ($connection->getGameMode() != 0) {
            $connection->forceEndRound();
            if (Maniaplanet::returnCallback($connection, "ManiaPlanet.EndRound")) {
                return redirect()->route('server.manage', $id)->with("warning", "Forced current round to end!");
            } else {
                return redirect()->route('server.manage', $id)->with("error", "Timeout");
            }
        } else {
            $connection->triggerModeScriptEvent('Trackmania.ForceEndRound', []);
            if (Maniaplanet::returnCallback($connection, "Maniaplanet.ChannelProgression_End")) {
                return redirect()->route('server.manage', $id)->with("warning", "Forced current round to end!");
            } else {
                return redirect()->route('server.manage', $id)->with("error", "Timeout");
            }
        }
    }

    public function cancelVote($id)
    {
        Maniaplanet::connect($id)->cancelVote();


        return redirect()->route('server.manage', $id)->with("warning", "Cancelled current vote!");
    }
}
