<?php

namespace App\Http\Controllers\Ajax;

use App\Helpers\Maniaplanet;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Manialib\Formatting\ManiaplanetString;
use Manialib\Gbx\Map;

class MapInfo extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        try {
            $map = Maniaplanet::connect($id)->getCurrentMapInfo();
            $map2 = Maniaplanet::connect($id)->getNextMapInfo();
            return view("server.ajax.mapInfo", compact('map', "map2"));
        } catch (\Exception $ex) {
            return response("Error: " . $ex->getMessage());
        }
    }

    public function maplist($id)
    {
        try {
            $maps = Maniaplanet::connect($id)->getMapList();
            return view("server.ajax.mapList", compact('maps'));
        } catch (\Exception $ex) {
            return response("Error: " . $ex->getMessage());
        }
    }

    public function showUpload($id)
    {
        try {
            return view("server.ajax.mapUpload", compact('id'));
        } catch (\Exception $ex) {
            return response("Error: " . $ex->getMessage());
        }
    }

    public function doUpload(Request $request, $id)
    {
        $input = $request->all();
        $rules = array(
            'file' => 'max:2000',
        );

        $validation = \Validator::make($input, $rules);

        if ($validation->fails()) {
            return response()->make($validation->errors->first(), 400);
        }

        $file = $request->file('file');
        $openedFile = $file->openFile("r");
        $data = $openedFile->fread($openedFile->getSize());

        try {
            Maniaplanet::fileAccess($id)->put('/UserData/Maps/' . $file->getClientOriginalName(), $data);
            //TODO: WriteFile() does not work on Linux - inquire with xbx
            //Maniaplanet::connect($id)->writeFile($file->getClientOriginalName(), base64_encode($data));
            Maniaplanet::connect($id)->addMap($file->getClientOriginalName());
            $map = Map::loadString($data);
            Maniaplanet::connect($id)->chatSendServerMessage('$z$s$fff#Admin$0cfServ $fff» [$7F7 Maps.Add$FFF] ' . $map->getName() . ' $z$s$FFFby ' . $map->getAuthor());
            return response()->json('success', 200);
        } catch (\Exception $ex) {
            return response()->json("error: " . $ex->getCode(), 400);
            Console.log($ex->getMessage());
        }
    }
}
