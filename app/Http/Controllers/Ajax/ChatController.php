<?php

namespace App\Http\Controllers\Ajax;

use App\Helpers\Maniaplanet;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Manialib\Formatting\ManiaplanetString;

class ChatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getChatLines($id)
    {
        $buffer = [];
        try {
            foreach (Maniaplanet::connect($id)->getChatLines() as $line) {
                $line = str_replace(['$<', '$>'], ["", '$ff0'], $line);
                $str = new ManiaplanetString($line);

                $buffer[] = htmlspecialchars((string)$str->stripLinks());
            }
            return response()->json($buffer, 200);
        } catch (\Exception $ex) {
            return response()->json([$ex->getMessage()], 200);
        }

    }

    public function sendChatLine(Request $request, $id)
    {
        try {
            $nick = Auth::user()->nickname;
            Maniaplanet::connect($id)->chatSendServerMessage('$fff#Admin$0cfServ $fff» $ff0[$fff' . $nick . '$z$s$ff0] $z$s' . $request->get('chat'));
            return response()->json("success", 200);
        } catch (\Exception $ex) {
            return response()->json("fail", 400);
        }
    }

}
