<?php

namespace App\Http\Controllers;

use App\Server;
use App\User;
use App\Role;
use Illuminate\Http\Request;

class UsersServerController extends Controller
{

    public function index(Request $request, $uid)
    {
        $perPage = 15;
        $user = User::find($uid);
        $servers = Server::getUserServers($uid)->orderBy('id', 'ASC')->paginate($perPage);
        return view('admin.users.server.index', compact('servers', "user", 'uid'))
            ->with('i', ($request->input('page', 1) - 1) * $perPage);
    }

    public function assign(Request $request, $uid)
    {
        $perPage = 15;
        $servers = Server::on()->orderBy('id', 'ASC')->paginate($perPage);
        return view('admin.users.server.assign', compact('servers', 'uid'))
            ->with('i', ($request->input('page', 1) - 1) * $perPage);
    }

    public function add(Request $request, $uid)
    {
        User::find($uid)->servers()->attach($request->get("serverId"));
        return redirect()->route("users.servers", $uid);

    }

    public function remove(Request $request, $uid)
    {
        User::find($uid)->servers()->detach($request->get("serverId"));
        return redirect()->route("users.servers", $uid);
    }

    public function roles(Request $request, $uid, $sid)
    {
        $perPage = 15;
        $roles = Role::orderBy('id', 'ASC')->where('server_id', $sid)->paginate($perPage);
        return view('server.roles.assign', compact('sid', 'uid', 'roles'))
            ->with('i', ($request->input('page', 1) - 1) * $perPage);
    }

}
