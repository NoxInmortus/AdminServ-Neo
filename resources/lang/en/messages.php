<?php

return [

    // General messages that reappear everywhere
    'welcome' => 'Welcome to #AdminServ!',
    'submit' => 'Submit',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'create' => 'Create',
    'cancel' => 'Cancel',
    'home' => 'Home',
    'servers' => 'Servers',
    'users' => 'Users',
    'manage' => 'Manage',
    'back' => 'Back',
    'logout' => 'Logout',
    'login' => 'Login',
    'setting-name' => 'Setting name',
    'value' => 'Value',
    'select-player' => 'Select player',
    'transfer' => 'Transfer',
    'number' => 'No.',
    'name' => 'Name',
    'description' => 'Description',
    'action' => 'Action',
    'add' => 'Add',
    'yes' => 'Yes',
    'no' => 'No',
    'role' => 'Role',
    'roles' => 'Roles',
    'save-changes' => 'Save changes',

    // Add/edit servers
    'add-new-server' => 'Add New Server',
    'add-a-server' => 'Add a Server',
    'server-name' => 'Server Name',
    'server-host' => 'Host',
    'server-xml-rpc-port' => 'XML-RPC Port',
    'server-xml-rpc-user' => 'XML-RPC User (SuperAdmin)',
    'server-xml-rpc-pwd' => 'XML-RPC Password',
    'server-uses-ftp' => 'Server uses FTP remote file storage (only enable this if host isn\'t localhost)',
    'server-uses-sftp' => 'Server uses SFTP',
    'server-ftp-remote-host' => '(S)FTP Host',
    'server-ftp-remote-port' => '(S)FTP Port',
    'server-ftp-remote-user' => '(S)FTP User',
    'server-ftp-remote-pass' => '(S)FTP Pass',

    // Server Manage Page and sub pages
    'server-manage-overview' => 'Overview',
    'server-manage-settings' => 'Settings',
    'server-manage-game-settings' => 'Game Settings',
    'server-manage-server-options' => 'Server Options',
    'server-manage-match-settings' => 'Match Settings',
    'server-manage-script-settings' => 'Script Settings',
    'server-manage-planets' => 'Planets',
    'server-manage-adminserv-specific' => 'AdminServ Specific',
    'server-manage-roles-management' => 'Roles Management',

    // Planets page
    'server-manage-planets-message' => '{0} The server currently has no planets|{1} The server currently has :value planet.|{2,*} The server currently has :value planets.',
    'server-manage-planets-transfer' => 'Transfer planets',
    'server-manage-planets-amount' => 'Choose amount of planets',
    'server-manage-planets-send-bill' => 'Send bill to player',
    'server-manage-planets-bill-msg' => 'Enter a message for the bill',
    'server-manage-planets-bill-create' => 'Create bill',

    // User management
    'user-server-assignment' => 'Server Assignment',
    'user-server-assign' => 'Assign Server',
    'user-server-role-assign' => 'Assign Role',
    'user-server-role-unassign' => 'Unassign Role',
    'user-server-unassign-confirm' => 'Do you really want to revoke access to this server for the user?',
    'user-create' => 'Create a User',
    'user-mp-login' => 'Maniaplanet Login',
    'user-email' => 'E-Mail',
    'user-email-placeholder' => 'Valid and working E-Mail address',
    'user-password' => 'Password',
    'user-password-confirm' => 'Confirm Password',
    'user-ui-lang' => 'Default Interface Language',
    'user-edit' => 'Editing User',
    'user-management' => 'User management',
    'user-delete-confirm' => 'Do you really want to delete this user?',
    'user-reset-password' => 'Reset your Password',
    'user-reset-password-send-link' => 'Send Password Reset Link',
    'user-login-remember' => 'Remember me',
    'user-login-maniaconnect' => 'Login via ManiaPlanet',
    'user-forgot-password' => 'Forgot your password?',








];
