<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminServ-Neo - @yield('title')</title>
    <meta name="description"
          content="AdminServ is an easy-to-use web interface to control your TMForever and ManiaPlanet servers.">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/semantic/semantic.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('/css/animate.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('/css/style.css') }}">
    <link href="{{ URL::asset('/css/toastr.css') }}" rel="stylesheet"/>
    @stack('style')

</head>
<body>
@if( session()->has('info'))
    <script>toastr["info"]('{{ session()->get('info')}}');</script>
@endif
@if(session()->has('success'))
    <script>toastr["success"]('{{ session()->get('success')}}');</script>
@endif
@if(session()->has('warning'))
    <script>toastr["warning"]('{{ session()->get('warning')}}');</script>
@endif
@if(session()->has('error'))
    <script>toastr["error"]('{{ session()->get('error')}}');</script>
@endif

@yield('content')


<script>
    $('.message .close')
        .on('click', function () {
            $(this)
                .closest('.message')
                .transition('fade');
        });
</script>
<script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<script src="{{URL::asset('/semantic/semantic.min.js') }}"></script>
<script src="{{ URL::asset('/js/toastr.min.js') }}"></script>
@stack('scripts')
{{ \DebugBar::enable() }}

</body>
