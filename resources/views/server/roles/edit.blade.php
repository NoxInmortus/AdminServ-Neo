@extends('layouts.menuapp')

@section('title', 'Edit Role')

@section('content')
    <div class="ui very padded raised container segment">
        <div class="ui grid">
            <div class="row">
                <div class="eight wide column">
                    <h2 class="ui header">Editing Role: {{$role->display_name}}</h2>
                </div>
            </div>
            <div class="row">
                <div class="sixteen wide column">
                    {!! Form::model($role, ['method' => 'PATCH', 'route' => array('roles.update', $sid, $role->id), 'class' => 'ui form']) !!}
                    <div class="required field {{$errors->first("display_name", "error")}}">
                        <label class="label">Name</label>
                        {!! Form::text('display_name', null, array('placeholder' => 'Name of the role', 'class' => 'form-control')) !!}
                    </div>
                    <div class="field">
                        <label class="label">Description</label>
                        {!! Form::text('description', null, array('placeholder' => 'Description of the role', 'class' => 'form-control')) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="sixteen wide column">
                    <h5 class="ui dividing header"><strong>Permissions</strong></h5>
                </div>
            </div>
            <div class="row">
                <div class="eight wide column">
                    <div class="field" name="permissions">
                        @for($i = 0; $i < sizeof($permission); $i++)
                            @if($i == 16)
                    </div>
                </div>
                <div class="eight wide column">
                    <div class="field">
                            @endif
                            @if(substr($permission[$i]->name, 0, 7) != 'global-')

                            <div class="ui slider checkbox">
                            {!! Form::checkbox('permission[]', $permission[$i]->id, in_array($permission[$i]->id, $rolePermissions->toArray()) ? true : false, array('class' => 'checkbox')) !!}
                                <label for="permission[]">{{$permission[$i]->display_name}}</label>
                            </div><br/><br/>
                            @endif
                        @endfor
                    </div>
                </div>
            <div class="field">
                <button class="ui green icon labeled button"><i class="checkmark icon"></i> Apply</button>
                <a class="ui button" href="{{route('roles.index', $sid) }}"><span>Cancel</span></a>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
