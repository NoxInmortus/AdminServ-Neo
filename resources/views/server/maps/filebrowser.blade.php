<?php
/** @var \Manialib\Gbx\Map $map */
?>
@extends("layouts.menuapp")

@section('title', 'File Browser')

@section('content')
    <div class="ui very padded raised container segment">
        <div class="ui grid">
            <div class="row">
                <div class="eight wide column">
                    <h2 class="ui header">File Browser</h2>
                </div>
                <div class="eight wide column right aligned">
                    <a class="ui black labeled icon button" href="{{ route('server.manage', [$id]) }}"><i
                                class="reply icon"></i><span>Back</span></a>
                </div>
            </div>
            <div id="maps" class="row">
                <div class="ui list">
                    @if ($path != ".")
                        <a class="ui item" href="{{ route("server.maps.filebrowser", [$id]) }}?path={{dirname($path)}}">
                            <i class="folder open icon"></i>
                            <div class="content">
                                <div class="header">(Parent directory)</div>
                            </div>
                        </a>
                    @endif
                    @foreach($dirs as $file)
                        <a class="ui item"
                           href="{{ route("server.maps.filebrowser", [$id]) }}?path={{$path}}/{{$file}}">
                            <i class="folder open icon"></i>
                            <div class="content">
                                <div class="header">{{$file}}</div>
                            </div>
                        </a>
                    @endforeach
                    @foreach($maps as $map)
                        <div class="ui item">
                            <i class="map icon"></i>
                            <div class="content">
                                <div class="header">{!! Maniaplanet::toHtml($map->getName()) !!}</div>
                                <div class="description">
                                    Author: {{$map->getAuthor()}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

@endpush
