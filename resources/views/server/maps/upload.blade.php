<?php
/** @var \Maniaplanet\DedicatedServer\Structures\Map $map */
?>
@extends("layouts.menuapp")

@section('title', 'Maps')

@section('content')
    <div class="ui very padded raised container segment">
        <div class="ui grid">
            <div class="row">
                <div class="eight wide column">
                    <h2 class="ui header">Maps & MX</h2>
                </div>
                <div class="eight wide column right aligned">
                    <a class="ui black labeled icon button" href="{{ route('server.manage', [$id]) }}"><i
                                class="reply icon"></i><span>Back</span></a>
                </div>
            </div>
        </div>
        @if(\App\Server::findOrFail($id)->getAttributeValue("host") != "localhost" && \App\Server::findOrFail($id)->useRemote || \App\Server::findOrFail($id)->getAttributeValue("host") == "localhost" || \App\Server::findOrFail($id)->getAttributeValue("host") != "127.0.0.1" && \App\Server::findOrFail($id)->useRemote || \App\Server::findOrFail($id)->getAttributeValue("host") == "127.0.0.1")
            {!! Form::open(["route" => ["server.manage.mxinstall", $id], "class" => "ui form"]) !!}
            <div class="ui fields">
                <div class="field">
                    <select name="gametype" class="ui dropdown">
                        <option value="TM">TM</option>
                        <option value="SM">SM</option>
                    </select>
                </div>
                <div class="field required">
                    <input type="text" name="mxid" placeholder="Mania-exchange map id"/>
                </div>
                <div class="field">
                    <button class="ui green inverted button" type="submit">Install</button>
                </div>

            </div>
            {!! Form::close()!!}
            @include('partials.upload', ["id" =>$id])
        @else
            <div class="ui error message">
                <i class="close icon"></i>
                <div class="header">
                    Map uploading and installing maps from Mania-Exchange are not available!
                </div>
                Your server seems to be running on a remote host with no FTP/SFTP file storage configured.<br>
                Due to ManiaPlanet API limitations, please setup FTP/SFTP remote file storage. You can do so <a href="{{ route('server.edit', [$id]) }}">here</a>.

            </div>
        @endif
    </div>
@endsection

@push('scripts')
<!--<script>
    $('#maps').sortable({
        containerSelector: 'table',
        itemPath: '> tbody',
        itemSelector: 'tr',
        placeholder: '<tr class="placeholder" />'
    });
</script>-->
@endpush
