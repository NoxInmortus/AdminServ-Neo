@if ($paginator->hasPages())
    <div class="ui center aligned basic segment">
        <div class="ui pagination menu">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <div class="disabled item"><span>&laquo;</span></div>
            @else
                <a class="item" href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <div class="disabled item"><span>{{ $element }}</span></div>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <a class="active item"><span>{{ $page }}</span></a>
                        @else
                            <a class="item" href="{{ $url }}">{{ $page }}</a>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <a href="{{ $paginator->nextPageUrl() }}" rel="next" class="item">&raquo;</a>
            @else
                <div class="disabled item"><span>&raquo;</span></div>
            @endif
        </div>
    </div>
@endif
