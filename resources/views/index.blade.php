<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminServ Redesign</title>
    <meta name="description"
          content="AdminServ is an easy-to-use web interface to control your TMForever and ManiaPlanet servers.">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.4.1/css/bulma.css" rel="stylesheet"
          integrity="sha256-PELQzdZwUQw2WSX3q4QLMSzDqQyWrmrXODp2bZy6JOU=" crossorigin="anonymous">
    <link href="/css/landing.css" rel="stylesheet">
</head>
<body>
<section class="hero is-fullheight is-dark">
    <div class="hero-head">
        <div class="container">
            <nav class="nav">
                <div class="container">
                    <div class="nav-left">
                        <a class="nav-item" href="/index">
                            <img src="/images/logo2.png" alt="Description">
                        </a>
                    </div>
                    <div class="nav-right nav-menu">
                        <a class="nav-item">
                            Configure
                        </a>
                        <span class="nav-item">
                        <a class="button is-default">
                            Choose Server
                        </a>
                        </span>
                    </div>
                </div>
            </nav>
        </div>
    </div>

    <div class="hero-body">
        <div class="container">
            <div class="columns">
                <div class="column is-4 is-offset-1">
                    <div class="card">
                        <header class="card-header">
                            <p class="card-header-title">
                                Chris92.de Test - Stadium
                            </p>
                            <a class="card-header-icon">
                      <span class="icon">
                        <i class="fa fa-car" title="Trackmania² Stadium"></i>
                      </span>
                            </a>
                        </header>
                        <div class="card-content">
                            <div class="content">
                                <div class="columns">
                                    <div class="column is-1">
                                        <i class="fa fa-gamepad"></i>
                                        <i class="fa fa-tv"></i>
                                        <i class="fa fa-map"></i>
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <div class="column is-5">
                                        Players:<br/>
                                        Spectators:<br/>
                                        Map:<br/>
                                        Uptime:<br/>
                                    </div>
                                    <div class="column is-5 has-text-right">
                                        0 / 32<br/>
                                        0 / 16<br/>
                                        Battle Royale 2K17<br/>
                                        2:34:56<br/>
                                    </div>
                                    <div class="column is-1 has-text-centered">
                                        <i class="fa fa-lock" title="Requires password"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="card-footer">
                            <a class="card-footer-item">Join</a>
                            <a class="card-footer-item">Spectate</a>
                            <a class="card-footer-item">Administrate</a>
                        </footer>
                    </div>
                </div>
                <div class="column is-4 is-offset-1">
                    <div class="card">
                        <header class="card-header">
                            <p class="card-header-title">
                                Chris92.de Test - Royal
                            </p>
                            <a class="card-header-icon">
                      <span class="icon">
                        <i class="fa fa-rocket" title="ShootMania Royal"></i>
                      </span>
                            </a>
                        </header>
                        <div class="card-content">
                            <div class="content">
                                <div class="columns">
                                    <div class="column is-1">
                                        <i class="fa fa-gamepad"></i>
                                        <i class="fa fa-tv"></i>
                                        <i class="fa fa-map"></i>
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <div class="column is-5">
                                        Players:<br/>
                                        Spectators:<br/>
                                        Map:<br/>
                                        Uptime:<br/>
                                    </div>
                                    <div class="column is-5 has-text-right">
                                        0 / 32<br/>
                                        0 / 16<br/>
                                        Battle Royale 2K17<br/>
                                        2:34:56<br/>
                                    </div>
                                    <div class="column is-1 has-text-centered">
                                        <i class="fa fa-lock" title="Requires password"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="card-footer">
                            <a class="card-footer-item">Join</a>
                            <a class="card-footer-item">Spectate</a>
                            <a class="card-footer-item">Administrate</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hero-foot">
        <div class="container">
            <div class="tabs is-centered">
                <ul>
                    <li><a href="http://bulma.io">Made with bulma</a></li>
                    <li><a>Based on Kev717's AdminServ</a></li>
                    <li>&copy; 2017 by Chris92 & reaby</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
</body>
</html>
