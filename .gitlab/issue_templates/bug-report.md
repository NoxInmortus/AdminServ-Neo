#### READ THIS BEFORE YOU SUBMIT AN ISSUE

When submitting a bug report, please add brief information about your setup where you run AdminServ.

**Required information:**
>- OS
>- Web server and version (e.g. Apache 2.4)
>- PHP version, if possible attach a file containing the output of ```phpinfo()``` or `php -i`

**Describe your issue in detail!**

A good example would be:

> I wanted to go to the overview of my server and I get "Error: cannot open socket - Maybe your server is offline?" in the Server Info box. 


If Laravel shows `Whoops, looks like something went wrong` -  do the following:
>1. Navigate to the root folder of your AdminServ-Neo installation
>2. Open the `.env` file - you may have to enable `Show hidden files` in your (S)FTP client
>3. Change `APP_DEBUG=false `to `APP_DEBUG=true` and save the changes
>4. Reload the page, if your browser asks you if you want to re-submit form data, hit yes
>5. Copy-paste the entire Stacktrace output into a text file and attach it to the issue

Also, please add reproduction steps, e.g. below:
> 1. Click on Servers in the navigation bar at the top
> 2. Click Manage next to a server